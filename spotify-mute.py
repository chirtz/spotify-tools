#!/usr/bin/env python
# -*- coding: utf-8 -*-
# DOES NOT WORK WITH NEW VERSIONS OF SPOTIFY
# Tested with 0.8.4

import dbus
import time, datetime
import os, sys, logging
import subprocess
from dbus.mainloop.glib import DBusGMainLoop
import gobject

# Keywords that indicate ads
keywords = [u"http", u":app", u"spotify"]

# At startup, the bus is not online
startup = True 
busOnline = False

fileSwitch = False
playbackStatus = "Paused"
record = False
recorder = None # Current recorder process
metaData = dict()
outdir = None
foldering = False
logg = False

####
# Other recording methods:
# MONITOR=$(pactl list | grep -A2 '^Source #' | grep 'Name: .*\.monitor$' | awk '{print $NF}' | tail -n1)
# parec -d "$MONITOR" | sox -t raw -r 44100 -b 16 -L -e signed -c 2 - -C192 "$OUTFILE"
####

# Writes a log message to the shell
def log(msg):
	global logg
	
	print "[WRAPPER] %s" % msg
	if logg:
		logging.debug(msg)


# Returns the PulseAudio connection ID for a specified program
def findPulseID(name):
	ID = -1
	SINK = -1
	sinks = os.popen("pactl list sink-inputs").readlines()
	for line in sinks:
		if any(x in line for x in ["Sink Input #", "Sink-Eingabe #"]):
			ID = line[line.index("#")+1:].strip()
		if "Sink:" in line:
			SINK = line[line.index(":")+1:].strip()
		if name.lower() in line.lower():
			log("PA ID found: %s" % str(ID))
			return (ID, SINK)
	log ("No PA ID found :(")
	return (-1,-1)


# Sets the correct monitor for aplay to record a specified program
def setMonitor(name, SINK):
	ID = -1
	MONITOR = None
	sinks = os.popen("pactl list").readlines()
	# Search for the Monitor Source of the Sink
	for line in sinks:
		if "Sink #" in line:
			ID = line[line.index("#")+1:].strip()
		if (ID == SINK and any(x in line for x in ["Monitor Source", "Quellenüberwachung"])):
			MONITOR = line[line.index(":")+1:].strip()
			log("Monitor found %s" % MONITOR)
			break

	if (MONITOR == None):
		log("No monitor found :(")
		log("Please set recording source manually")
		return

	OUTID = -1
	# Search for the ID of the Source Output of aplay
	for line in sinks:
		if any( x in line for x in ["Source Output #", "Quellausgabe"]):
			OUTID = line[line.index("#")+1:].strip()
		if "aplay".lower() in line.lower():
			log("aplay ID found: %s" % str(OUTID))
			break

	if (OUTID == -1):
		log("No aplay ID found :(")
		log("Please set recording source manually")
		return

	# Set the output source with pactl:
	setter = subprocess.Popen(("pactl","move-source-output",str(OUTID), MONITOR))
	setter.wait()


# Replaces non-valid characters for a file - or pathname in a string
def normalizeString (string):
	rules = [("/"," "),("\\"," ")]

	tmp = string
	for (old, new) in rules:
		tmp = tmp.replace(old,new)

	return tmp

# Starts the recording of the audio to one of the temporary files
def recordAudio():
	global fileSwitch
	global recorder
	global outdir

	if (not fileSwitch):
		musicFile = outdir+'tmp.wav'
	else:
		musicFile = outdir+'tmp2.wav'


	#ps = subprocess.Popen(('arecord', '-f', 'cd', '-d', str(length), '-t', 'wav'), stdout=open(musicFile, 'w'))
	recorder = subprocess.Popen(('arecord', '-f', 'cd', '-t', 'wav'), stdout=open(musicFile, 'w'))
	fileSwitch = not fileSwitch	

# Stops the recording of the audio, if recording
def stopRecorder():
	global recorder
	if (recorder != None):
		recorder.terminate()
		recorder.wait()
		recorder = None
		# transcode recording
		transcode()

# transcodes the temporary wav file into a mp3, if it is not an ad
def transcode():
	global fileSwitch
	global metaData
	global outdir
	global foldering

	# Ignore ad's
	if (metaData.get('ad', False) == True):
		return

	if (fileSwitch):
		musicFile = outdir+'tmp.wav'
	else:
		musicFile = outdir+'tmp2.wav'

	length = int(metaData.get('length',"0"))
	if (length <= 0):
		return
	length = length / 1000000
	title = metaData.get('title',"")
	artist = metaData.get('artist',"")
	album = metaData.get('album',"")
	number = metaData.get('number',"0")

	artistN = normalizeString(artist)
	albumN = normalizeString(album)
	titleN = normalizeString(title)

	if foldering:
		# Create folders for ordering: Folder named Interpret -> Subfolder named Album -> Files named with trackid and title
		try:
			os.mkdir(outdir+artistN+"/")
		except:
			#Folder already exists
			pass

		try:
			os.mkdir(outdir+artistN+"/"+albumN+"/")
		except:
			#Folder already exists
			pass

	# Transcode to mp3 with lame and add the id3 tags
	command = []
	command.append('lame')
	# Set up the id3 tags with parameters
	command.append('--tt')
	command.append(title)

	command.append('--ta')
	command.append(artist)

	command.append('--tl')
	command.append(album)

	# Year has to be extracted: command.extend('--ty', metaData.get('contentCreated',""))
	command.append('--tn')
	command.append(metaData.get('number',"0"))

	command.append('--tc')
	command.append(metaData.get('artURL',""))

	# I/O Files:
	command.append(musicFile)

	if foldering:
		command.append(outdir+artistN+"/"+albumN+"/"+ number + '-' + titleN +'.mp3')
	else:
		command.append(outdir+ titleN + '-' + artistN + '-' + albumN +'.mp3')

	# Check for already existing files: If the file exists => Return and do not replace it!
	if (os.path.exists(command[-1])):
		return

	try:
		log("Start Encoding...")
		subprocess.Popen(command)
#		output.wait()
	except:
		pass

# Gets the Meta of the current song and checks, if it is a ad
def getMeta(info):
	global metaData

	# Log the complete information, which is Available:
	artist = unicode(info.get('xesam:artist',[""])[0])
	title = unicode(info.get('xesam:title',""))
	album = unicode(info.get('xesam:album',""))
	number = unicode(info.get('xesam:trackNumber',""))
	discNumber = unicode(info.get('xesam:discNumber',""))
	trackid = unicode(info.get('mpris:trackid',""))
	length = unicode(info.get('mpris:length',""))
	artURL =  unicode(info.get('mpris:artUrl',""))
	autoRating = unicode(info.get('xesam:autoRating',""))
	contentCreated = unicode(info.get('xesam:contentCreated',""))
	url =  unicode(info.get('xesam:url',""))

	if (artist != "" and title != ""):
		metaData = {"artist":artist, "title":title, "album":album, "number":number,
			"discNumber":discNumber, "trackid":trackid, "length":length, "artURL":artURL,
			"autoRating":autoRating, "contentCreated":contentCreated, "url":url, "ad":False}
	else:
		# We do not know here if it is an add or a song played, where we get no meta infos.
		return


	# "Old" Check:
	# Search album name for ad keywords 
	if any (k.lower() in album for k in keywords):
		metaData['ad'] = True
		return

	# new Check if it is an ad
	nones = 0
	for meta in metaData.itervalues():
		if (meta==""):
			nones +=1
			if (nones >2):
				# Assume this is an ad!
				metaData['ad'] = True
				return

def on_start_up(wait=False, toMute=False):
	global pulseID
	global startup
	global record

	# If the first song is played or the pulseID changed, determine PA connection ID
	if startup or pulseID < 0:
		if wait:
			# Wait for PA
			log("First action, waiting 2 secs for PA before ID query")
			time.sleep(2)
		(pulseID, SINK) = findPulseID("spotify")
		if (record and SINK >= 0):
			setMonitor("spotify", SINK)
		# Fix for unmuting, if spotify was closed while muted
		if (pulseID >= 0):
			mute(toMute)
		startup = False
	
	
	
# Mutes or unmutes the sound of spotify
def mute(on):
	global pulseID
	global startup
	
	if startup or pulseID < 0:
		return
	
	if not on:
		setBit = 0
		# Wait a bit for the ad to finish
#		time.sleep(1)

	else:
		log("AD detected. Muting")
		setBit = 1

	#Unmute: os.system("pactl set-sink-input-mute %s 0" % pulseID)
	#Since sometimes the sink id changes, a check on the output of pactl should give us the information we need.
	#BEFORE: os.system("pactl set-sink-input-mute %s %i" % (pulseID, setBit) )
	out = subprocess.Popen(("pactl", "set-sink-input-mute", str(pulseID), str(setBit)))
	out.wait()

	if out.returncode != 0:
		log("PulseID changed, trying reload")
		startup = True
		pulseID = -1
		on_start_up(toMute=on)
	#os.system("amixer sset 'Master' unmute > /dev/null")

# Called whenever a property is changed, e.g. when the player is paused
# or when the song changes
def handle_properties_changed(b, interface, changed_props, **kwargs):
	global playbackStatus
	global record
	global metaData

	if record:
		# Stop previous recording asap:
		stopRecorder()

	# Get current playback status: (if it is not given in the dict, the current status remains)
	playbackStatus = interface.get("PlaybackStatus", playbackStatus)


	if (playbackStatus == "Playing"):
		# Start next recording asap
		if record:
			recordAudio()
		# UnMute:
		mute(False)

	# Check if everything is set
	on_start_up(True)

	# Get the current meta information:
	info = interface.get("Metadata", {})
	getMeta(info)
	log("Current: "+ str(metaData))

	# Mute and return if it is an ad
	if metaData.get('ad', False):
		# If the player is currently playing:
		if (playbackStatus == "Playing"):
			# If player is not muted and we found an ad, mute
			mute(True)
		# Stop recording (ad):
		stopRecorder()




# Called when spotify is closed
# (or when the bus is initialized)
def handle_disconnect(newOwner):
	global busOnline
	if not busOnline:
		busOnline = True
	else:
		log("Disconnected -> Terminating...")
		loop.quit()


def setOutputOptions(path, folders=False):
	global outdir
	global foldering
	global record
	global logg
	
	outdir = path
	foldering = folders
	record = True
	log("Recording Audio enabled!")


if __name__ == "__main__":
	
	# Check for help and printing usage information:
	if (sys.argv[-1] in ['help','-help','--help','?','-?','--?','/?']):
		print("Usage: spotify [outdir] [Foldering]\n")
		print("[outdir]: \t If a directory is given, the sound of spotify is recorded \n \t \t and saved under the given directory (Default: No Recording)")
		print("[Foldering]: \t If False, the songs are saved in [outdir]: \n \t \t Files named 'title - artist - album. (Default: False)")
		print("\t \t If True, the songs are saved in subdirectories of [outdir]: \n \t \t Subdirectory 'Artist' -> Subdirectory 'Album' -> Files named 'Number - Title'.")
		exit(0)

	# Get outdir if given and set the options:
	if (len(sys.argv) > 2 and (sys.argv[2] in ['true', 'True', 'foldering', 'Foldering'])):
		setOutputOptions(sys.argv[1], True)
	elif (len(sys.argv) > 1):
		setOutputOptions(sys.argv[1])
	try:
		if os.path.exists('/var/log/spotify/'):
			logging.basicConfig(level=logging.DEBUG,
					format='%(asctime)s %(levelname)s %(message)s',
					filename='/var/log/spotify/spotify.log',
					filemode='a')
			logg = True
	except:
		logg = False
	
	# Open given spotify client
	p = subprocess.Popen("spotify-original", shell=True)
	# Loop setup
	bus_loop = DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus(mainloop=bus_loop)
	loop = gobject.MainLoop()

	while True:
		try:
			spotify = bus.get_object("org.mpris.MediaPlayer2.spotify", "/org/mpris/MediaPlayer2")
			spotify.connect_to_signal("PropertiesChanged", handle_properties_changed)
			log("Connection to Spotify client established")
			break
		except:
			time.sleep(2)
	bus.watch_name_owner('org.mpris.MediaPlayer2.spotify', handle_disconnect)
	loop.run()

	# Remove temporary files, if something has been recorded:
	if record:
		for path in (outdir+'tmp.wav', outdir+'tmp2.wav'):
			if (os.access(path, os.W_OK)):
				os.remove(path)

